<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', [
    'uses' => 'AuthController@login',
    'as' => 'login'
]);

Route::post('logout', [
    'uses' => 'AuthController@logout',
    'as' => 'logout'
]);

Route::post('refresh', [
    'uses' => 'AuthController@refresh',
    'as' => 'refresh'
]);

Route::post('user', [
    'uses' => 'AuthController@me',
    'as' => 'auth-user'
]);